DROP TABLE IF EXISTS `cdc_contact_form`;
CREATE TABLE IF NOT EXISTS `cdc_contact_form` (
    `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
    `full_name` varchar(100) NOT NULL,
    `email_address` varchar(255) NOT NULL,
    `message` text NOT NULL,
    `phone_number` varchar(100) DEFAULT NULL,
    `date_submitted` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
    PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
