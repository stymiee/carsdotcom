<?php

namespace Library\Validator;

use carsdotcom\Library\Validator\FullNameValidator;
use PHPUnit\Framework\TestCase;

class FullNameValidatorTest extends TestCase
{
    public function fullNameDataProvider(): array
    {
        return [
            ['John Smith', true],
            ['Cher', true],
            [' x ', true],
            ['', false],
            [' ', false],
        ];
    }

    /**
     * @dataProvider fullNameDataProvider
     * @param string $fullName
     * @param bool $isValid
     */
    public function testIsValid(string $fullName, bool $isValid): void
    {
        $fullNameValidator = new FullNameValidator();
        self::assertEquals($isValid, $fullNameValidator->isValid($fullName));
    }
}
