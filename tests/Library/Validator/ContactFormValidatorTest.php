<?php

namespace Library\Validator;

use carsdotcom\Library\Validator\ContactFormValidator;
use carsdotcom\Library\Validator\EmailAddressValidator;
use carsdotcom\Library\Validator\FullNameValidator;
use carsdotcom\Library\Validator\MessageValidator;
use carsdotcom\Library\Validator\PhoneNumberValidator;
use PHPUnit\Framework\TestCase;
use ReflectionProperty;

class ContactFormValidatorTest extends TestCase
{
    public function testGetErrorMessagesReturnValue(): void
    {
        $errorMessages = [
            'emailaddress' => 'Invalid email address',
            'fullname' => 'Please enter your full human name',
            'message' => 'Please add a message to send to the team',
            'phonenumber' => 'Please enter a phone number in the correct format',
        ];

        $contactFormValidator = new ContactFormValidator();

        $errorMessagesProperty = new ReflectionProperty($contactFormValidator, 'errorMessages');
        $errorMessagesProperty->setAccessible(true);
        $errorMessagesProperty->setValue($contactFormValidator, $errorMessages);

        self::assertEquals($errorMessages, $contactFormValidator->getErrorMessages());
    }

    public function errorMessagesDataProvider(): array
    {
        return [
            [[], true, true, true, true],
            [['emailaddress' => 'Invalid email address'], false, true, true, true],
            [['fullname' => 'Please enter your full human name'], true, false, true, true],
            [['message' => 'Please add a message to send to the team'], true, true, false, true],
            [['phonenumber' => 'Please enter a phone number in the correct format'], true, true, true, false],
            [['emailaddress' => 'Invalid email address',
              'phonenumber' => 'Please enter a phone number in the correct format'], false, true, true, false],
            [['emailaddress' => 'Invalid email address',
              'fullname' => 'Please enter your full human name',
              'message' => 'Please add a message to send to the team',
              'phonenumber' => 'Please enter a phone number in the correct format'], false, false, false, false],
        ];
    }

    /**
     * @dataProvider errorMessagesDataProvider
     * @param array $errorMessages
     * @param bool $emailIsValid
     * @param bool $fullNameIsValid
     * @param bool $messageIsValid
     * @param bool $phoneNumberIsValid
     */
    public function testGetErrorMessages(
        array $errorMessages,
        bool $emailIsValid,
        bool $fullNameIsValid,
        bool $messageIsValid,
        bool $phoneNumberIsValid
    ): void {
        $emailAddressValidator = $this->getMockBuilder(EmailAddressValidator::class)
            ->getMock();
        $emailAddressValidator->method('isValid')
            ->willReturn($emailIsValid);

        $fullNameValidator = $this->getMockBuilder(FullNameValidator::class)
            ->getMock();
        $fullNameValidator->method('isValid')
            ->willReturn($fullNameIsValid);

        $messageValidator = $this->getMockBuilder(MessageValidator::class)
            ->disableOriginalConstructor()
            ->getMock();
        $messageValidator->method('isValid')
            ->willReturn($messageIsValid);

        $phoneNumberValidator = $this->getMockBuilder(PhoneNumberValidator::class)
            ->getMock();
        $phoneNumberValidator->method('isValid')
            ->willReturn($phoneNumberIsValid);

        $contactFormValidator = new ContactFormValidator();
        $contactFormValidator->setEmailAddressValidator($emailAddressValidator);
        $contactFormValidator->setFullNameValidator($fullNameValidator);
        $contactFormValidator->setMessageValidator($messageValidator);
        $contactFormValidator->setPhoneNumberValidator($phoneNumberValidator);
        $contactFormValidator->isValid(['phonenumber' => 'x']);

        self::assertEquals($errorMessages, $contactFormValidator->getErrorMessages());
    }

    public function contactFormDataProvider(): array
    {
        return [
            [true, true, true, true, true],
            [false, false, true, true, true],
            [false, true, false, true, true],
            [false, true, true, false, true],
            [false, true, true, true, false],
        ];
    }

    /**
     * @dataProvider contactFormDataProvider
     * @param bool $formIsValid
     * @param bool $emailIsValid
     * @param bool $fullNameIsValid
     * @param bool $messageIsValid
     * @param bool $phoneNumberIsValid
     */
    public function testIsValid(
        bool $formIsValid,
        bool $emailIsValid,
        bool $fullNameIsValid,
        bool $messageIsValid,
        bool $phoneNumberIsValid
    ): void {
        $emailAddressValidator = $this->getMockBuilder(EmailAddressValidator::class)
            ->getMock();
        $emailAddressValidator->method('isValid')
            ->willReturn($emailIsValid);

        $fullNameValidator = $this->getMockBuilder(FullNameValidator::class)
            ->getMock();
        $fullNameValidator->method('isValid')
            ->willReturn($fullNameIsValid);

        $messageValidator = $this->getMockBuilder(MessageValidator::class)
            ->disableOriginalConstructor()
            ->getMock();
        $messageValidator->method('isValid')
            ->willReturn($messageIsValid);

        $phoneNumberValidator = $this->getMockBuilder(PhoneNumberValidator::class)
            ->getMock();
        $phoneNumberValidator->method('isValid')
            ->willReturn($phoneNumberIsValid);

        $contactFormValidator = new ContactFormValidator();
        $contactFormValidator->setEmailAddressValidator($emailAddressValidator);
        $contactFormValidator->setFullNameValidator($fullNameValidator);
        $contactFormValidator->setMessageValidator($messageValidator);
        $contactFormValidator->setPhoneNumberValidator($phoneNumberValidator);

        self::assertEquals($formIsValid, $contactFormValidator->isValid(['phonenumber' => 'x']));
    }
}
