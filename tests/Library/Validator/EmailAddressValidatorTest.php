<?php

namespace Library\Validator;

use carsdotcom\Library\Validator\EmailAddressValidator;
use PHPUnit\Framework\TestCase;

class EmailAddressValidatorTest extends TestCase
{
    public function emailAddressDataProvider(): array
    {
        return [
            ['test@example.com', true],
            ['@example.com', false],
            ['test@.com', false],
            ['test@example', false],
        ];
    }

    /**
     * @dataProvider emailAddressDataProvider
     * @param string $emailAddress
     * @param bool $isValid
     */
    public function testIsValid(string $emailAddress, bool $isValid): void
    {
        $emailAddressValidator = new EmailAddressValidator();
        self::assertEquals($isValid, $emailAddressValidator->isValid($emailAddress));
    }
}
