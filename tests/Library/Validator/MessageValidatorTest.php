<?php

namespace Library\Validator;

use carsdotcom\Library\Validator\MessageValidator;
use PHPUnit\Framework\TestCase;

class MessageValidatorTest extends TestCase
{
    public function messageDataProvider(): array
    {
        return [
            [str_repeat('x', 25), 25, true],
            [str_repeat('x', 10), 10, true],
            [str_repeat('x', 10), 25, false],
            [str_repeat('x', 24), 25, false],
            ['  ' . str_repeat('x', 24) . '  ', 25, false],
            ['x' . str_repeat(' ', 24) . 'x', 25, true],
        ];
    }

    /**
     * @dataProvider messageDataProvider
     * @param string $message
     * @param int $minimumLength
     * @param bool $isValid
     */
    public function testIsValid(string $message, int $minimumLength, bool $isValid): void
    {
        $messageValidator = new MessageValidator($minimumLength);
        self::assertEquals($isValid, $messageValidator->isValid($message));
    }
}
