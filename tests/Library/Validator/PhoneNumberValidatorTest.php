<?php

namespace Library\Validator;

use carsdotcom\Library\Validator\PhoneNumberValidator;
use PHPUnit\Framework\TestCase;

class PhoneNumberValidatorTest extends TestCase
{
    public function phoneNumberDataProvider(): array
    {
        return [
            ['555-555-5555', true],
            ['555 555 5555', false],
            ['555-5555', false],
            ['(555) 555-5555', false],
            ['+44 7911 123456', false],
            ['phone number', false],
        ];
    }

    /**
     * @dataProvider phoneNumberDataProvider
     * @param string $phoneNumber
     * @param bool $isValid
     */
    public function testIsValid(string $phoneNumber, bool $isValid): void
    {
        $phoneNumberValidator = new PhoneNumberValidator();
        self::assertEquals($isValid, $phoneNumberValidator->isValid($phoneNumber));
    }
}
