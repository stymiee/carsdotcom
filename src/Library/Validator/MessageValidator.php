<?php

declare(strict_types=1);

namespace carsdotcom\Library\Validator;

class MessageValidator implements IValidator
{
    private int $minimumLength;

    public function __construct(int $minimumLength)
    {
        $this->minimumLength = $minimumLength;
    }

    /**
     * Just validates minimum length of a message not including leading and trailing spaces as that is the least likely
     * to miss valid data. A better validation can be created based upon specific business rules.
     *
     * @param $value
     * @return bool
     */
    public function isValid($value): bool
    {
        $fullName = trim($value);
        return strlen($fullName) >= $this->minimumLength;
    }
}
