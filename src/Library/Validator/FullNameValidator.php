<?php

declare(strict_types=1);

namespace carsdotcom\Library\Validator;

class FullNameValidator implements IValidator
{
    /**
     * Validating a full name is virtually impossible due to the numerous rules many languages and cultures have for
     * names. Just make sure it isn't empty for now. Could always be expanded to account for country, culture,
     * language, etc.
     *
     * @param string $value
     * @return bool
     */
    public function isValid($value): bool
    {
        $fullName = trim($value);
        return !empty($fullName);
    }
}
