<?php

declare(strict_types=1);

namespace carsdotcom\Library\Validator;

interface IValidator
{
    public function isValid($value): bool;
}
