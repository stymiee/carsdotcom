<?php

declare(strict_types=1);

namespace carsdotcom\Library\Validator;

class EmailAddressValidator implements IValidator
{
    /**
     * Validates the email address is in a valid format. Future updates could allow for validating:
     * - MX record is configured (risky)
     * - Email provider is not a free provider
     * - Email provider is not a disposable email provider
     * - Deny list for blocked domain names
     *
     * @param $value
     * @return bool
     */
    public function isValid($value): bool
    {
        $emailAddress = trim($value);
        return (bool) filter_var($emailAddress, FILTER_VALIDATE_EMAIL);
    }
}
