<?php

declare(strict_types=1);

namespace carsdotcom\Library\Validator;

class ContactFormValidator
{
    private array $errorMessages = [];

    private EmailAddressValidator $emailAddressValidator;
    private FullNameValidator $fullNameValidator;
    private MessageValidator $messageValidator;
    private PhoneNumberValidator $phoneNumberValidator;

    /**
     * In a more robust system will have a Validator Interface (or abstract class) and will build the validator by
     * dynamically adding validators so this could be a loop instead a bunch of IF statements.
     *
     * @param array $formValues
     * @return bool
     */
    public function isValid(array $formValues): bool
    {
        $isValid = true;

        $emailAddress = $formValues['emailaddress'] ?? '';
        if (!$this->emailAddressValidator->isValid($emailAddress)) {
            $isValid = false;
            $this->addErrorMessage('emailaddress', 'Invalid email address');
        }
        $fullName = $formValues['fullname'] ?? '';
        if (!$this->fullNameValidator->isValid($fullName)) {
            $isValid = false;
            $this->addErrorMessage('fullname', 'Please enter your full human name');
        }
        $message = $formValues['message'] ?? '';
        if (!$this->messageValidator->isValid($message)) {
            $isValid = false;
            $this->addErrorMessage('message', 'Please add a message to send to the team');
        }
        $phoneNumber = $formValues['phonenumber'] ?? '';
        if ($phoneNumber && !$this->phoneNumberValidator->isValid($phoneNumber)) {
            $isValid = false;
            $this->addErrorMessage('phonenumber', 'Please enter a phone number in the correct format');
        }

        return $isValid;
    }

    public function getErrorMessages(): array
    {
        return $this->errorMessages;
    }

    public function setEmailAddressValidator(EmailAddressValidator $emailAddressValidator): void
    {
        $this->emailAddressValidator = $emailAddressValidator;
    }

    public function setFullNameValidator(FullNameValidator $fullNameValidator): void
    {
        $this->fullNameValidator = $fullNameValidator;
    }

    public function setMessageValidator(MessageValidator $messageValidator): void
    {
        $this->messageValidator = $messageValidator;
    }

    public function setPhoneNumberValidator(PhoneNumberValidator $phoneNumberValidator): void
    {
        $this->phoneNumberValidator = $phoneNumberValidator;
    }

    private function addErrorMessage(string $key, string $message): void
    {
        $this->errorMessages[$key] = $message;
    }

}
