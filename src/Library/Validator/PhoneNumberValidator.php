<?php

declare(strict_types=1);

namespace carsdotcom\Library\Validator;

class PhoneNumberValidator implements IValidator
{
    /**
     * Validates phone number is in XXX-XXX-XXXX format as directed in the form. Future update could allow for
     * flexible or configurable formats.
     *
     * @param $value
     * @return bool
     */
    public function isValid($value): bool
    {
        $phoneNumber = trim($value);
        return (bool) preg_match("/^([1]-)?[\d]{3}-[\d]{3}-[\d]{4}$/", $phoneNumber);
    }
}
