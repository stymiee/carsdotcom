<?php

declare(strict_types=1);

namespace carsdotcom\Controllers;

use carsdotcom\Library\Validator\ContactFormValidator;
use PDO;
use PHPMailer\PHPMailer\PHPMailer;
use Psr\Container\ContainerInterface;

class EmailController
{
    private array $config;
    private ContactFormValidator $contactFormValidator;
    private PDO $db;
    private PHPMailer $mailer;

    public function __construct(ContainerInterface $container)
    {
        $this->config = $container->get('config');
        $this->mailer = $container->get('mail');
        $this->db = $container->get('db');

        $this->contactFormValidator = $container->get('ContactFormValidator');
    }

    public function process($request, $response, $args)
    {
        $emailRequest = $request->getParsedBody();

        if (!$this->contactFormValidator->isValid($emailRequest)) {
            $errors = [
                'result' => 'error',
                'errors' => $this->contactFormValidator->getErrorMessages()
            ];
            $jsonResponse = json_encode($errors, JSON_THROW_ON_ERROR);
            $response->getBody()->write($jsonResponse);
            return $response
                ->withHeader('Content-Type', 'application/json');
        }

        try {
            $phoneNumber = $emailRequest['phonenumber'];
            $emailBody = sprintf(
                'Name: %s\nEmail Address: %s\nPhone Number: %s\nMessage: %s',
                $emailRequest['fullname'],
                $emailRequest['emailaddress'],
                $phoneNumber,
                $emailRequest['message'],
            );

            $this->mailer->Subject = "Email Subject";
            $this->mailer->Body = $emailBody;
            $this->mailer->AddAddress($this->config['recipients']['contact']);
            $this->mailer->send();

            $sql = 'INSERT INTO `cdc_contact_form`
                    (`full_name`, `email_address`, `message`, `phone_number`)
                    VALUES (:full_name, :email_address, :message, :phone_number)';
            $stmt = $this->db->prepare($sql);
            $stmt->execute(
                [
                    ':full_name'     => $emailRequest['fullname'],
                    ':email_address' => $emailRequest['emailaddress'],
                    ':message'       => $emailRequest['message'],
                    ':phone_number'  => $emailRequest['phonenumber'] ?? null,
                ]
            );

            $success = [
                'result' => 'success',
                'errors' => []
            ];
            $jsonResponse = json_encode($success, JSON_THROW_ON_ERROR);
        } catch (\Exception $e) {
            $errors = [
                'result' => 'error',
                'errors' => []
            ];
            $jsonResponse = json_encode($errors, JSON_THROW_ON_ERROR);
        } finally {
            $response->getBody()->write($jsonResponse);
            return $response
                ->withHeader('Content-Type', 'application/json');
        }
    }
}
