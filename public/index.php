<?php

declare(strict_types=1);

use carsdotcom\Controllers\EmailController;
use carsdotcom\Library\Validator\ContactFormValidator;
use carsdotcom\Library\Validator\EmailAddressValidator;
use carsdotcom\Library\Validator\FullNameValidator;
use carsdotcom\Library\Validator\MessageValidator;
use carsdotcom\Library\Validator\PhoneNumberValidator;
use DI\Container;
use PHPMailer\PHPMailer\PHPMailer;
use Psr\Container\ContainerInterface;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;
use Slim\Factory\AppFactory;
use Slim\Views\Twig;
use Slim\Views\TwigMiddleware;
use Twig\Loader\FilesystemLoader;

session_start();

require __DIR__ . '/../vendor/autoload.php';
$config = require __DIR__ . '/../env.php';

$container = new Container();
$container->set('config', static function () use ($config) {
    return $config;
});
$container->set('db', static function () use ($config) {
    $db = $config['database'];
    $dsn = sprintf('mysql:host=%s;dbname=%s;port=%s', $db['host'], $db['name'], $db['port']);
    $pdo = new PDO($dsn, $db['user'], $db['pass']);
    $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    $pdo->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_ASSOC);
    return $pdo;
});
$container->set('mail', static function () use ($config) {
    $mail = new PHPMailer();
    $mail->IsSMTP();
    $mail->SMTPDebug = 0;
    $mail->SMTPAuth = true;
    $mail->SMTPSecure = 'ssl';
    $mail->Host = $config['email']['host'];
    $mail->Port = $config['email']['port'];
    $mail->Username = $config['email']['address'];
    $mail->Password = $config['email']['password'];
    $mail->SetFrom($config['email']['address']);
    return $mail;
});
$container->set('view', static function () use ($config) {
    $loader = new FilesystemLoader($config['views']);
    return new Twig($loader, []);
});

$container->set('EmailController', static function (ContainerInterface $c) {
    return new EmailController($c);
});
$container->set('ContactFormValidator', static function (ContainerInterface $c) use ($config) {
    $contactFormValidator = new ContactFormValidator();
    $contactFormValidator->setEmailAddressValidator(new EmailAddressValidator());
    $contactFormValidator->setFullNameValidator(new FullNameValidator());
    $contactFormValidator->setMessageValidator(new MessageValidator($config['validators']['message']['minimumLength']));
    $contactFormValidator->setPhoneNumberValidator(new PhoneNumberValidator());
    return $contactFormValidator;
});

/*
 * In a more robust application we would add middleware to catch uncaught exceptions and add logging to all requests.
 */
AppFactory::setContainer($container);
$app = AppFactory::create();
$app->add(TwigMiddleware::createFromContainer($app));

$app->get('/', function (Request $request, Response $response, $args) {
    return $this->get('view')->render($response, 'index.html');
});
$app->post('/process', EmailController::class . ':process');
$app->get('/thankyou', function (Request $request, Response $response, $args) {
    return $this->get('view')->render($response, 'thankyou.html');
});
$app->run();
